var height = 500;
var width = 900;
var margins = {
    left: 40,
    top: 40,
    right: 40,
    bottom: 40
}
/////////////

var svg = d3.select("svg")
    .attr("width", width)
    .attr("height", height)

var xscale = d3.scaleLinear()
    .domain([0,10])
    .range([0,width-margins.left-margins.right]);
var yscale = d3.scaleLinear()
    .domain([0,40])
    .range([height-margins.top-margins.bottom,0]);
var xaxis = d3.axisBottom(xscale)
    .ticks(10);
var yaxis = d3.axisLeft(yscale)
    .ticks(10);

d3.select('svg').append("g")
    .attr("transform","translate("+margins.left+","+margins.top+")")
    .call(yaxis);
d3.select('svg').append("g")
    .attr("transform","translate("+margins.left+","+(height-margins.bottom)+")")
    .call(xaxis);

var barWidth = Math.floor((width-margins.left-margins.right)/ 12 );

/////////////////

function showdata(data, selectcity) {
    //
    d3.select('svg').select('#textinfo').remove()
    //
    svg = d3.select("svg").selectAll('rect').data(data)
    var g = svg.enter().append('g')
    /////
    showstyle(d3.select("svg").append('text'), 'title',selectcity)
    //////////
    /////
    showstyle(svg, 'bar')
    showstyle(g.append('rect'), 'bar')
    ///////
    showstyle(g.append('text'), 'language')
    showstyle(d3.select('svg').selectAll('text[name=text1]').data(data),'language')
    ///////
    showstyle(g.append('text'), 'percent')
    showstyle(d3.select('svg').selectAll('text[name=text2]').data(data), 'percent')
}

function showstyle(obj,action,selectcity) {
    // add this to all
    obj = obj.transition()
        .duration(2000)
    //
    var x = 0

    if(action === 'title') {
        obj.text('PopularitY of Programming Language of 2019 - '+ selectcity)
            .attr('fill', 'black')
            .attr('x', width / 2)
            .attr('y', 30)
            .attr('text-anchor', 'middle')
            .attr('id', 'textinfo')
            .style('font-size', '15px')
    }

    if (action === 'percent') {
        obj.text(d => d[1] + '%')
            .attr('fill', 'white')
            .attr('x', () => {
                x++
                return xscale(x) + 37.5
            })
            .attr('y', d => ((yscale(0) + 35) - yscale(40 - d[1]) + 20))
            .attr('text-anchor', 'middle')
            .attr('name', 'text2')
            .style('font-size', '12px')
    }

    if (action === 'language') {
        obj.text(d => d[0])
            .attr('fill', 'red')
            .attr('x', () => {
                x++
                return xscale(x) + 37.5
            })
            .attr('y', d => (yscale(0) + 35) - yscale(40 - d[1]))
            .attr('text-anchor', 'middle')
            .attr('name', 'text1')
            .style('font-size', '12px')
    }

    if (action === 'bar') {
        obj.attr('x', () => {
            x++
            return xscale(x) + 5
        })
            .attr('y', d => (yscale(0) + 40) - yscale(40 - d[1]))
            .attr('width', barWidth)
            .attr('height', d => yscale(40 - d[1]))
            .attr('margin-left', '5px')
    }
}

showdata(all_data['World_Wide'], 'World Wide')

function changeData() {
    var obj = document.getElementById('city_select')
    var index = obj.selectedIndex
    var sel = obj.options[index].text
    showdata(all_data[sel], sel)
}
